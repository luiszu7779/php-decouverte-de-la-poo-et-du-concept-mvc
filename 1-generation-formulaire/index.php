<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body>
    <div class="container mt-5">
        <?php
        require 'vendor/autoload.php';

        use App\Validator\Validator;

        if ($_POST) {
            $validated = (new Validator($_POST))->validate([
                'firstname' => 'required|min:3|max:30',
                'description' => 'required|min:10|max:255',
                'animal' => 'required|is:dog,cat'
            ], [
                'firstname.required' => 'Le champ firstname est requis.',
                'firstname.min' => 'Le champ first name doit faire min 3 chars.',
                'firstname.max' => 'Le champ first name doit faire max 30 chars.',
                'description.required' => 'Le champ description est requis',
                'description.min' => 'La description doit faire plus de 10 chars.',
                'description.max' => 'La description doit faire max 255 chs.',
                'animal.required' => 'Le champ animal est requis',
                'animal.is' => 'Le champ animal comporte une valeur qui n\'est pas autorisée.'
            ]);
            
            if (is_array($validated)) {
                ?>
                    <div class="alert alert-success" role="alert"> Formulaire validé </div>
                <?php
            }
        }

        if (isset($_SESSION['errors'])) {
            foreach ($_SESSION['errors'] as $error) {
        ?>
            <div class="alert alert-danger" role="alert"> <?= $error ?> </div>
        <?php
            }
        }

        if (isset($_SESSION['success'])) {
            foreach ($_SESSION['success'] as $success) {
        ?>
            <div class="alert alert-success" role="alert"> <?= $success ?> </div>
        <?php
            }
        }

        use App\Form;

        $form = new Form('/1-generation-formulaire/index.php', 'POST');
        $form->input('firstname', '', [
            'label' => 'Prénom',
            'id' => 'firstname',
            'class' => 'form-control',
            'type' => 'text',
        ]);
        $form->textarea('description', 'test', [
            'class' => 'form-control',
            'label' => 'Description',
            'id' => 'description',
            'label-class' => 'mt-3'
        ]);
        $form->select(
            'animal',
            [
                'dog' => 'Dog',
                'cat' => 'Cat'
            ],
            [
                'class' => 'form-select',
                'id' => 'animal',
                'label' => 'Votre animal:',
                'label-class' => 'mt-3'
            ]
        );
        $form->button('submit', 'Valider', [
            'type' => 'submit',
            'class' => 'btn btn-success mt-3'
        ]);
        echo $form->build();
        ?>
    </div>
</body>

</html>