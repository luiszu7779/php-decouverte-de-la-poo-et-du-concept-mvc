<?php

namespace App;

trait SessionMessagesTrait
{
    private function throwNewFlashError(string $error)
    {
        $_SESSION['errors'][] = $error;
    }

    private function throwNewFlashSuccess(string $success)
    {
        $_SESSION['success'][] = $success;
    }
}