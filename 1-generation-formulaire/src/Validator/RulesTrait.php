<?php

namespace App\Validator;

trait RulesTrait
{
    public function required(mixed $value)
    {
        if (is_null($value) || strlen($value) === 0) {
            return false;
        }

        return true;
    }

    public function max(int|string $value, int $max)
    {
        if (is_int($value)) return $value <= $max;

        return strlen($value) <= $max;
    }

    public function min(int|string $value, int $min)
    {
        if (is_int($value)) return $value >= $min;

        return strlen($value) >= $min;
    }

    public function is(int|string $value, string $authorizedValues)
    {
        $authorizedValues = explode(',', $authorizedValues);

        return in_array($value, $authorizedValues);
    }
}