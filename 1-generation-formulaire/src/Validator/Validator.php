<?php

namespace App\Validator;
use App\SessionMessagesTrait;

class Validator
{
    use SessionMessagesTrait;
    use RulesTrait;

    public array $data;
    public array $validatedData = [];
    public array $rules;
    public bool $hasErrors = false;
    public array $messages = [];

    public function __construct(array $data)
    {
        $this->setData($data);   
    }

    private function setData(array $data): void
    {
        $this->data = $data;
    }

    public function validate(array $rules, array $messages): array|false
    {
        $this->setRules($rules);
        $this->setMessages($messages);

        foreach ($this->data as $name => $value) {
            if (! array_key_exists($name, $this->rules)) {
                $this->throwNewFlashError("Erreur: le champ '$name' ne devrait pas être présent.");
                $this->hasErrors = true;
                continue;
            }

            if ($this->verifyRulesHaveErrors($name, $value)) $this->hasErrors = true;

            $this->setValidatedData([$name => $value]);
        }

        return ! $this->hasErrors ? $this->getValidatedData() : false;
    }

    /**
     * Retourne true si des erreurs sont levées
     * @param string $name
     * @param mixed $value
     * @return bool
     */
    private function verifyRulesHaveErrors(string $name, mixed $value): bool
    {
        $hasErrors = false;

        foreach ($this->rules as $field => $rules) {
            if ($field !== $name) continue;

            $explodedRules = explode('|', $rules);
            foreach ($explodedRules as $rule) {
                if (str_contains($rule, ':')) {
                    if (! $this->verifyRuleWithArgs($rule, $value, $field)) $hasErrors = true;
                    continue;
                }

                if (! $this->verifyRuleWithoutArgs($rule, $value, $field)) $hasErrors = true;
            }
        }

        return $hasErrors;
    }

    private function verifyRuleWithArgs(string $rule, mixed $value, string $field): bool
    {
        $rule = explode(':', $rule);

        if (! method_exists($this, $rule[0])) {
            throw new \ErrorException("La règle de validation $rule[0] n'éxiste pas!");
        }

        if ($this->{$rule[0]}($value, $rule[1])) {
            return true;
        }

        $this->findAndFlashErrorMessage($field, $rule[0]);
        return false;
    }

    private function verifyRuleWithoutArgs(string $rule, mixed $value, string $field): bool
    {
        if (! method_exists($this, $rule)) {
            throw new \ErrorException("La règle de validation $rule n'éxiste pas!");
        }

        if ($this->{$rule}($value)) {
            return true;
        }

        $this->findAndFlashErrorMessage($field, $rule);
        return false;
    }

    private function setValidatedData(array $data)
    {
        $this->validatedData = array_merge($this->validatedData, $data);
    }

    private function getValidatedData()
    {
        return $this->validatedData;
    }

    private function setRules(array $rules)
    {
        $this->rules = $rules;
    }

    private function setMessages(array $messages)
    {
        $this->messages = $messages;
    }

    private function findAndFlashErrorMessage(string $name, string $ruleNotRespected)
    {
        if (isset($this->messages[$name . '.' . $ruleNotRespected])) {
            $this->throwNewFlashError($this->messages[$name . '.' . $ruleNotRespected]);
            return;
        }

        throw new \ErrorException("Le message d'erreur n'a pas été configuré pour la règle $ruleNotRespected");
    }
}