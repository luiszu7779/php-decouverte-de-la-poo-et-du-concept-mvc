<?php
namespace App;

use App\FormElements\Button;
use App\FormElements\Input;
use App\FormElements\Select;
use App\FormElements\TextArea;

final class Form
{
    private string $html = '';

    public function __construct(string $action, string $method)
    {
        $html = "<form action='$action' method='$method'>";
        $this->addHtml($html);
    }

    public function input(string $name, string|array $value, array $options): void
    {
        $this->addHtml(new Input($name, $value, $options));
    }

    public function button(string $name, string|array $value, array $options): void
    {
        $this->addHtml(new Button($name, $value, $options));
    }

    public function textarea(string $name, string|array $value, array $options): void
    {
        $this->addHtml(new TextArea($name, $value, $options));
    }

    /**
     * @param string $name
     * @param array|null $values
     * Exemple $values = [
     *      'dog' => 'Dog',
     *      'cat' => 'Cat',
     *      '{value}' => '{text shown}'
     * ]
     * @return void
     */
    public function select(string $name, string|array $value, array $options): void
    {
        $this->addHtml(new Select($name, $value, $options));
    }

    private function addHtml(string $html): void
    {
        $this->html .= $html;
    }

    private function getHtml(): string
    {
        return $this->html;
    }

    public function build(): string
    {
        $this->addHtml("</form>");

        return $this->getHtml();
    }
}