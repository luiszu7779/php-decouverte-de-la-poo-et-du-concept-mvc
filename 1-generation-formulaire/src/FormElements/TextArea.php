<?php

namespace App\FormElements;

class TextArea extends Element implements ElementInterface
{
    public function build(): void
    {
        $this->addHtml("<textarea name='" . $this->getName() . "' ");

        $this->getClass() ? $this->addHtml("class='" . $this->getClass() . "' ") : null;
        $this->getId() ? $this->addHtml("id='" . $this->getId() . "' ") : null;

        $this->addHtml(">" . $this->getValue() . "</textarea>");
    }
}