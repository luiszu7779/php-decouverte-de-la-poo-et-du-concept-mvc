<?php

namespace App\FormElements;
 
interface ElementInterface
{
    public function build(): void;
}