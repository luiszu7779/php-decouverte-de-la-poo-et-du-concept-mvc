<?php

namespace App\FormElements;

class Input extends Element implements ElementInterface
{
    public function build(): void
    {
        $this->addHtml("<input name='" . $this->getName() . "' ");

        $this->getValue() ? $this->addHtml("value='" . $this->getValue() . "' ") : null;
        $this->getType() ? $this->addHtml("type='" . $this->getType() . "' ") : null;
        $this->getId() ? $this->addHtml("id='" . $this->getId() . "' ") : null;
        $this->getClass() ? $this->addHtml("class='" . $this->getClass() . "' ") : null;
        $this->getRequired() ? $this->addHtml("required ") : null;
        $this->getPlaceholder() ? $this->addHtml("placeholder='" . $this->getPlaceholder() . "' ") : null;

        $this->addHtml(">");
    }
}