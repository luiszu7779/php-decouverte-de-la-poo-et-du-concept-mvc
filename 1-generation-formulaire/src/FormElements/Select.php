<?php

namespace App\FormElements;

class Select extends Element implements ElementInterface
{
    public function build(): void
    {
        $this->addHtml("<select name='" . $this->getName() . "' ");

        $this->getClass() ? $this->addHtml("class='" . $this->getClass() . "' ") : null;

        $this->addHtml(">");

        foreach ($this->getValue() as $value => $text) {
            $this->addHtml("<option value='$value'>$text</option>");
        }

        $this->addHtml("</select>");
    }
}