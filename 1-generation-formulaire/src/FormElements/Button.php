<?php

namespace App\FormElements;

class Button extends Element implements ElementInterface
{
    public function build(): void
    {
        $this->addHtml("<button ");

        $this->getType() ? $this->addHtml("type='" . $this->getType() . "' ") : null;
        $this->getId() ? $this->addHtml("id='" . $this->getId() . "' ") : null;
        $this->getClass() ? $this->addHtml("class='" . $this->getClass() . "' ") : null;

        $this->addHtml(">" . $this->getValue() . "</button>");
    }
}