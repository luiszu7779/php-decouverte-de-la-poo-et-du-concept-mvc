<?php 

namespace App\FormElements;

abstract class Element
{
    public string $name;
    public string|array $value;
    public string $type;
    public string $id;
    public string $class;
    public bool $required;
    public string $placeholder;
    public string $label;
    public string $labelClass;
    public string $html = '';

    public function __construct(string $name, null|string|array $value = '', ? array $options = [])
    {
        $this->setName($name);
        $this->setValue($value ?? '');
        $this->setType($options['type'] ?? '');
        $this->setClass($options['class'] ?? '');
        $this->setId($options['id'] ?? '');
        $this->setRequired($options['required'] ?? false);
        $this->setPlaceholder($options['placeholder'] ?? '');
        $this->setLabel($options['label'] ?? '');
        $this->setLabelClass($options['label-class'] ?? '');

        $this->build();

        if ($this->getLabel() && $this->getId()) {
            $label = "<label for='" . $this->getId() . "' ";
            $this->getLabelClass() ? $label .= "class='" . $this->getLabelClass() . "' " : null;
            $label .= ">" . $this->getLabel() . "</label>";
            $this->addHtmlToStart($label);
        }
    }

    /**
     * Remplacé par la méthode build de l'enfant
     * @return void
     */
    public function build(): void{}

    private function setName(string $name): void
    {
        $this->name = $name;
    }

    protected function getName(): string
    {
        return $this->name;
    }

    private function setValue(string|array $value): void
    {
        $this->value = $value;
    }

    protected function getValue(): string|array
    {
        return $this->value;
    }

    private function setType(string $type): void
    {
        $this->type = $type;
    }

    protected function getType(): string
    {
        return $this->type;
    }

    private function setId(string $id): void
    {
        $this->id = $id;
    }

    protected function getId(): string
    {
        return $this->id;
    }

    private function setClass(string $class): void
    {
        $this->class = $class;
    }

    protected function getClass(): string
    {
        return $this->class;
    }

    private function setRequired(bool $required): void
    {
        $this->required = $required;
    }

    protected function getRequired(): bool
    {
        return $this->required;
    }

    private function setPlaceholder(string $placeholder): void
    {
        $this->placeholder = $placeholder;
    }

    protected function getPlaceholder(): string
    {
        return $this->placeholder;
    }

    private function setLabel(string $label): void
    {
        $this->label = $label;
    }

    protected function getLabel(): string
    {
        return $this->label;
    }

    private function setLabelClass(string $labelClass): void
    {
        $this->labelClass = $labelClass;
    }

    protected function getLabelClass(): string
    {
        return $this->labelClass;
    }

    protected function addHtml(string $html): void
    {
        $this->html .= $html;
    }

    protected function addHtmlToStart(string $html): void
    {
        $this->html = $html . $this->html;
    }

    protected function getHtml(): string
    {
        return $this->html;
    }

    public function __toString(): string
    {
        return $this->getHtml();
    }
}