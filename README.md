---
title: PHP - La découverte de la POOen créant notre propre MVC.
metadata:
  level : second_year, third_year
  language: php
---
# Exercices PHP -La Programmation orientée objet

Il vous est demandé de forker ce dépot, réaliser le travail demandé puis de faire une merge request en fin de journée.
Tâchez d'aller le plus loin possible...Ce travail servira de base pour le travail de la journée suivante.
Nous ferons un point en début d'après-midi puis en fin de journée.

> Rappel : un fichier PHP ne s'ouvre dans le navigateur en faisant un glisser déposer ! Ça ne va pas fonctionner !
Vous devez absolument passer par votre serveur apache local : http://localhost/ ou http://localhost:8000/ ou etc.
> Voire en lançant un serveur depuis le dossier par le biais de la commande : `php -S localhost:8000`.

# Etape 1
Tout se trouve dans le dossier [1-generation-formulaire](./1-generation-formulaire)

Bon courage à tou.te.s

# Etape 2
Un dossier `classes`est disponible au sein du dépôt. Il s'agit de la correction apportée ensemble.
 Je ne vous demande pas de réaliser un copier coller mais de le réécrire et de le comprendre puis implémentez une nouvelle méthode dans le form : `addSelectField($fieldName, $fieldValue, $options)`

Ce n'est pas pour vous punir, loin de là, mais vous seriez dépassés par l'étape d'après sinon.

Pour les autres, des modifications ont été apportées par le biais d'un commit. Lisez les et attaquez le dossier [2-refactoring-de-classe](./2-refactoring-de-classe).
Bon courage à tout.te.s !
